import dateXmlProvider from '../../../providers/xml/date';
import { IRoute } from '../../router';

export const route: IRoute = {
	url: '/date',
	method: 'POST',
	handler: (req, res) => dateXmlProvider(res),
};

export default route;
