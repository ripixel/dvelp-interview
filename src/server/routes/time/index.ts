import timeXmlProvider from '../../../providers/xml/time';
import { IRoute } from '../../router';

export const route: IRoute = {
	url: '/time',
	method: 'POST',
	handler: (req, res) => timeXmlProvider(req, res),
};

export default route;
