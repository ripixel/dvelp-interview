import { IRoute } from '../../router';

export const route: IRoute = {
	url: '/test',
	method: 'GET',
	handler: (req, res) => res.send('The server is alive!'),
};

export default route;
