import guestsXmlProvider from '../../../providers/xml/guests';
import { IRoute } from '../../router';

export const route: IRoute = {
	url: '/guests',
	method: 'POST',
	handler: (req, res) => guestsXmlProvider(req, res),
};

export default route;
