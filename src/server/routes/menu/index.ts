import menuXmlProvider from '../../../providers/xml/menu';
import { IRoute } from '../../router';

export const route: IRoute = {
	url: '/menu',
	method: 'POST',
	handler: (req, res) => menuXmlProvider(res),
};

export default route;
