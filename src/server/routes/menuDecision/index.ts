import menuDecisionXmlProvider from '../../../providers/xml/menuDecision';
import { IRoute } from '../../router';

export const route: IRoute = {
	url: '/menuDecision',
	method: 'POST',
	handler: (req, res) => menuDecisionXmlProvider(req, res),
};

export default route;
