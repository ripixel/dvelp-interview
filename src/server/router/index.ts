import { Express, RequestHandler } from 'express-serve-static-core';

export interface IRoute {
	url: string;
	method: 'POST' | 'GET';
	handler: RequestHandler;
}

export const router = (app: Express, routes: IRoute[]) => {
	routes.forEach((route) => {
		switch (route.method) {
			case 'POST':
				app.post(route.url, route.handler);
				break;
			default:
				app.use(route.url, route.handler);
				break;
		}
	});
};

export default router;
