import { router } from './index';

describe('Server > Router', () => {
	test('Adds expected routes', () => {
		const mockApp = {
			post: jest.fn(),
			use: jest.fn(),
		};

		const routes = [
			{
				url: '/test',
				method: 'NOT A THING',
				handler: 'testFunction',
			},
			{
				url: '/shouldBePost',
				method: 'POST',
				handler: 'shouldBePostFunction',
			},
		];

		router(mockApp as any, routes as any);

		// /test
		expect(mockApp.use.mock.calls[0][0]).toEqual(routes[0].url);
		expect(mockApp.use.mock.calls[0][1]).toEqual(routes[0].handler);
		// /shouldBePost
		expect(mockApp.post.mock.calls[0][0]).toEqual(routes[1].url);
		expect(mockApp.post.mock.calls[0][1]).toEqual(routes[1].handler);
	});
});
