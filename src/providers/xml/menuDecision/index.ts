import {
	Request as IRequest,
	Response as IResponse,
} from 'express-serve-static-core';

import dateXmlProvider from '../date';
import menuXmlProvider from '../menu';

export const menuDecisionXmlProvider = (req: IRequest, res: IResponse) => {
	res.setHeader('content-type', 'application/xml');
	const digitSelected = Number.parseInt(req.body.Digits, 10);
	const speechResult: string = req.body.SpeechResult;

	// digits take precedence
	if (digitSelected) {
		switch (digitSelected) {
			case 1:
				// Make a booking, begin the chain
				return dateXmlProvider(res);
			default:
				// Do the menu again
				return menuXmlProvider(res);
		}
	}

	if (speechResult && speechResult.toLowerCase().indexOf('booking')) {
		return dateXmlProvider(res);
	}

	// Nothing has been recognised - do the menu again
	return menuXmlProvider(res);
};

export default menuDecisionXmlProvider;
