import { guestsXmlProvider } from './index';

describe('Providers > Xml > Time', () => {
	test('Produces expected output', () => {
		const mockResponse: any = {
			setHeader: jest.fn(),
			send: (xml: any) => xml,
		};
		const mockRequest: any = {
			body: {
				Digits: 6,
			},
		};

		expect(guestsXmlProvider(mockRequest, mockResponse)).toMatchSnapshot();
	});

	test('Sets content-type correctly', () => {
		const mockResponse: any = {
			setHeader: jest.fn(),
			send: (xml: any) => xml,
		};
		const mockRequest: any = {
			body: {
				Digits: 6,
			},
		};

		guestsXmlProvider(mockRequest, mockResponse);
		expect(mockResponse.setHeader.mock.calls[0][0]).toEqual('content-type');
		expect(mockResponse.setHeader.mock.calls[0][1]).toEqual(
			'application/xml'
		);
	});
});
