import {
	Request as IRequest,
	Response as IResponse,
} from 'express-serve-static-core';
import * as js2xmlparser from 'js2xmlparser';

export const guestsXmlProvider = (req: IRequest, res: IResponse) => {
	res.setHeader('content-type', 'application/xml');
	const digitsEntered = req.body.Digits;

	return res.send(
		js2xmlparser.parse('Response', {
			Say: {
				'@': {
					language: 'en-GB',
				},
				'#':
					'You entered ' +
					digitsEntered +
					' for your selection of time. Enter a number between 0 and 9.',
			},
			Hangup: {},
		})
	);
};

export default guestsXmlProvider;
