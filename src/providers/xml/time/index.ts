import {
	Request as IRequest,
	Response as IResponse,
} from 'express-serve-static-core';
import * as js2xmlparser from 'js2xmlparser';

export const timeXmlProvider = (req: IRequest, res: IResponse) => {
	res.setHeader('content-type', 'application/xml');
	const digitsEntered = req.body.Digits;

	return res.send(
		js2xmlparser.parse('Response', {
			Gather: {
				'@': {
					action: '/guests',
					timeout: '4',
					numDigits: '1',
				},
				"Say": {
					'@': {
						language: 'en-GB',
					},
					'#':
						'You entered ' +
						digitsEntered +
						' for your selection of date. Enter a number between 0 and 9.',
				},
			},
		})
	);
};

export default timeXmlProvider;
