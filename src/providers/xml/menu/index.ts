import { Response as IResponse } from 'express-serve-static-core';
import * as js2xmlparser from 'js2xmlparser';

export const menuXmlProvider = (res: IResponse) => {
	res.setHeader('content-type', 'application/xml');

	return res.send(
		js2xmlparser.parse('Response', {
			Gather: {
				'@': {
					action: '/menuDecision',
					timeout: '4',
					numDigits: '1',
					input: 'dtmf speech',
					speechModel: 'numbers_and_commands',
				},
				"Say": {
					'@': {
						language: 'en-GB',
					},
					'#':
						"Welcome to the Interview Cafe, please tell us why you're calling or press 1 to make a booking",
				},
			},
		})
	);
};

export default menuXmlProvider;
