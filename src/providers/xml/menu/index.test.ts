import { menuXmlProvider } from './index';

describe('Providers > Xml > Menu', () => {
	test('Produces expected output', () => {
		const mockResponse = {
			setHeader: jest.fn(),
			send: (xml) => xml,
		};

		expect(menuXmlProvider(mockResponse as any)).toMatchSnapshot();
	});

	test('Sets content-type correctly', () => {
		const mockResponse = {
			setHeader: jest.fn(),
			send: (xml) => xml,
		};

		menuXmlProvider(mockResponse as any);
		expect(mockResponse.setHeader.mock.calls[0][0]).toEqual('content-type');
		expect(mockResponse.setHeader.mock.calls[0][1]).toEqual(
			'application/xml'
		);
	});
});
