import { Response as IResponse } from 'express-serve-static-core';
import * as js2xmlparser from 'js2xmlparser';

export const dateXmlProvider = (res: IResponse) => {
	res.setHeader('content-type', 'application/xml');

	return res.send(
		js2xmlparser.parse('Response', {
			Gather: {
				'@': {
					action: '/time',
					timeout: '4',
					numDigits: '1',
				},
				"Say": {
					'@': {
						language: 'en-GB',
					},
					'#':
						'You are selecting a Date. Enter a number between 0 and 9',
				},
			},
		})
	);
};

export default dateXmlProvider;
