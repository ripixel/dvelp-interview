import { dateXmlProvider } from './index';

describe('Providers > Xml > Date', () => {
	test('Produces expected output', () => {
		const mockResponse = {
			setHeader: jest.fn(),
			send: (xml) => xml,
		};

		expect(dateXmlProvider(mockResponse as any)).toMatchSnapshot();
	});

	test('Sets content-type correctly', () => {
		const mockResponse = {
			setHeader: jest.fn(),
			send: (xml) => xml,
		};

		dateXmlProvider(mockResponse as any);
		expect(mockResponse.setHeader.mock.calls[0][0]).toEqual('content-type');
		expect(mockResponse.setHeader.mock.calls[0][1]).toEqual(
			'application/xml'
		);
	});
});
