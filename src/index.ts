import bodyParser from 'body-parser';
import express from 'express';

import router from './server/router';
import dateRoute from './server/routes/date';
import guestsRoute from './server/routes/guests';
import menuRoute from './server/routes/menu';
import menuDecisionRoute from './server/routes/menuDecision';
import testRoute from './server/routes/test';
import timeRoute from './server/routes/time';

console.log('\nStarting Dvelp Interview | ' + new Date().toTimeString());
const app = express();
app.use(bodyParser.urlencoded());

router(app, [
	dateRoute,
	timeRoute,
	guestsRoute,
	testRoute,
	menuRoute,
	menuDecisionRoute,
]);

const port = 4000;
app.listen(port);

console.log('Listening on port ' + port);
console.log('--------------------------------');
console.log('Server available at: http://localhost:' + port);
console.log('--------------------------------');
console.log('Started Dvelp Interview\n');
